<?php
//////////////////////////////////////////////////////////////
//===========================================================
// mods.php(For individual softwares)
//===========================================================
// SOFTACULOUS 
// Version : 1.0
// Inspired by the DESIRE to be the BEST OF ALL
// ----------------------------------------------------------
// Started by: Alons
// Date:       10th Jan 2009
// Time:       21:00 hrs
// Site:       http://www.softaculous.com/ (SOFTACULOUS)
// ----------------------------------------------------------
// Please Read the Terms of use at http://www.softaculous.com
// ----------------------------------------------------------
//===========================================================
// (c)Softaculous Inc.
//===========================================================
//////////////////////////////////////////////////////////////

if(!defined('SOFTACULOUS')){

	die('Hacking Attempt');

}


/**
 * This function will allow you to modify the XML that is being passed.
 * You can modify it here and Softaculous will parse it as it parses the XML of install.xml of packages.
 *
 * @package      softaculous
 * @subpackage   scripts
 * @author       Pulkit Gupta
 * @param        string $str The key of the Language string array.
 * @return       string The parsed string if there was a equivalent language key otherwise the key itself if no key was defined.
 * @since     	 1.0
 */
/*function __wp_mod_install_xml($xml){
	
	global $__settings, $settings, $error, $software, $globals, $softpanel, $notes, $adv_software;
	
}*/

/**
 * This function will parse your mod_install.xml and shows an option of choose plugin to users 
 *
 * @package      softaculous
 * @subpackage   scripts
 * @author       Pulkit Gupta
 * @param        string $str The key of the Language string array.
 * @return       string The parsed string if there was a equivalent language key otherwise the key itself if no key was defined.
 * @since     	 1.0
 */
function __wp_mod_settings(){
	
	global $__settings, $settings, $error, $software, $globals, $softpanel, $notes, $adv_software;
	
	$install = @implode(file($globals['path'].'/conf/mods/'.$software['softname'].'/mod_install.xml'));
	
	$install = parselanguages($install);
	
	$tmp_settings = array();
	
	if(preg_match('/<softinstall (.*?)>(.*?)<\/softinstall>/is', $install)){
		
		$tmp_settings = load_settings($install, $adv_software, 1);
	}
	
	$key = parselanguages('{{select_plugins}}');
		
	$settings[$key] = array_merge($settings[$key], $tmp_settings[$key]);
	
}

/**
 * If anything is needed to be execute before the installation procedure starts than it should be done here.
 *
 * @package      softaculous
 * @subpackage   scripts
 * @author       Pulkit Gupta
 * @param        string $str The key of the Language string array.
 * @return       string The parsed string if there was a equivalent language key otherwise the key itself if no key was defined.
 * @since     	 1.0
 */
 
function __pre_mod_install(){
	
	global $__settings, $settings, $error, $software, $globals, $softpanel, $notes, $adv_software;

}

/**
 * If anything is needed to be execute after the installation procedure starts than it should be done here.
 *
 * @package      softaculous
 * @subpackage   scripts
 * @author       Pulkit Gupta
 * @param        string $str The key of the Language string array.
 * @return       string The parsed string if there was a equivalent language key otherwise the key itself if no key was defined.
 * @since     	 1.0
 */
function __post_mod_install(){
	
	global $__settings, $settings, $error, $software, $globals, $softpanel, $notes, $adv_software;
	
	// Check which plugin is checked for instllation
	if(!empty($__settings['litespeed-cache'])){
		// Download LiteSpeed-Cache using wp-cli:)
		$litespeed_install = ('wp plugin install litespeed-cache --activate --path='. $__settings['softpath']);
		shell_exec($litespeed_install);

	}
	
}

?>
