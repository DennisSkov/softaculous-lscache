# README #

This allows you to automatically setup LSCache when provisioning WordPress websites using Softaculous in cPanel.
This mod for Softaculous will install LSCWP by default, unless the client removes the checkmark from the checkbox. This behavious can be changed. Read Configuration.

![Softaculous UI with mod installed](https://i.imgur.com/zrTitwB.png)

The LSCache plugin is installed using WP-CLI which handles the installation and activation.

### Dependencies ###
- [WP-CLI](https://make.wordpress.org/cli/)
- [Softaculous](https://www.softaculous.com/)
- [LiteSpeed](https://litespeedtech.com)

### Installation ###

1. `mkdir -p /usr/local/cpanel/whostmgr/cgi/softaculous/conf/mods/wp/ && cd /usr/local/cpanel/whostmgr/cgi/softaculous/conf/mods/wp/`
2. `git clone git@bitbucket.org:DennisSkov/softaculous-lscache.git`

### Configuration ###
Very little can be configured. You can change the text shown, the description and whether or not the checkbox should be checked by default.
All changes should be made in mod_install.xml.